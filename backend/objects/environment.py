import uuid
import random
from objects import GameObject


class Ground(GameObject):

    Sprites = {
        "bare-ground": [["images/sprites/ground/bare-ground.png", 0]],
        "grassed-ground": [["images/sprites/ground/grassed-ground.png", 0]],
        "rocked-ground": [["images/sprites/ground/rocked-ground.png", 0]]
    }

    def __init__(self):
        GameObject.__init__(self)
        self.sprites = Ground.Sprites
        self.current_sprite = Ground.Sprites.keys()[random.randint(0, 2)]


class Barrel(GameObject):

    Sprites = {"barrel": [["barrel.png", 0]]}

    @classmethod
    def Data(cls):
        return {
            "id": str(uuid.uuid1()),
            "object": "Barrel",
            "sprites": Barrel.Sprites,
            "type": "dynamic",
            "polygones": [
                [-5, -5],
                [5, -5],
                [5, 5],
                [5, -5]
            ]
        }

    @classmethod
    def create(cls, world):
        body = world.CreateDynamicBody(
            position=(random.random() * 800, random.random() * 600),
            userData=Barrel.Data()
        )
        body.CreatePolygonFixture(
            box=(1, 1),
            density=1,
            friction=0.3
        )
        return body
