Requirements
============

    * Linux (like Ubuntu)
    * Git
    * Python (gevent, greenlet, gunicorn, flask)
    * PyBox2D 2.1b

Installation
============
_(for ubuntu like systems)_

Installing requirements:
------------------------

        sudo apt-get install git python python-gevent python-greenlet python-pip python-flask 
        sudo pip install gunicorn gevent-websocket 

PyBox2D installation:
---------------------

[See instruction here](http://code.google.com/p/pybox2d/wiki/BuildingfromSource)

Gevent pywsgi.py fix:
---------------------
_Fix was brought to you by [stackoverflow.com](http://stackoverflow.com/questions/9444405/gunicorn-and-websockets)_

1. Find pywsgi.py file (usually it's located here: /usr/share/pyshared/gevent) and open to edit (as root)

2. Find this code:

		def log_request(self):
		    log = self.server.log
		    if log:
		        log.write(self.format_request() + '\n')

3. And replace to this:

		def log_request(self):
		    log = self.server.log
		    if log:
		        if hasattr(log, "info"):
		            log.info(self.format_request() + '\n')
		        else:
		            log.write(self.format_request() + '\n')

How-to run:
===========

1. Install requirements

2. Clone repository:
        
        git clone https://bitbucket.org/floatdrop/jsgame.git

3. Apply patches to python

        ./pathces/patch.sh

4. Run 

        ./run.sh

5. Go to [http://localhost/](http://localhost/) to start playing

Documentation
=============

Summary docs: [http://airlab.math.usu.ru/redmine/projects/jsgame/wiki](http://airlab.math.usu.ru/redmine/projects/jsgame/wiki)

Box2Dweb: [http://www.box2dflash.org/docs/2.1a/reference/](http://www.box2dflash.org/docs/2.1a/reference/)

Usefull links
-------------

Echo server: [https://gist.github.com/1185629](https://gist.github.com/1185629)
