// Main javascript file
//

websocket = new WebSocket("ws://" + window.location.host + "/" + window.location.pathname)

websocket.onmessage = function(json_message)
{
    message = JSON.parse(json_message.data)
    switch (message.command) {
        case "redirect":
          window.location = message.url;
          break
    }
}
